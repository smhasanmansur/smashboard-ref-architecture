# Results from Partition - Index Experiment #


## READ (AGGREGATION) ##

1) SELECT COUNT(*) AS `reply_count` FROM replies  USE INDEX (created_time)  WHERE created BETWEEN '2017-05-01 00:00:00' AND '2017-05-31 23:59:59' AND account_id IN (SELECT account_id from account_circles WHERE circle_id = 1) ORDER BY id ASC

2) SELECT COUNT(*) AS `reply_count` FROM replies PARTITION(Jun2017) USE INDEX (created_time)  WHERE created BETWEEN '2017-05-01 00:00:00' AND '2017-05-31 23:59:59' AND account_id IN (SELECT account_id from account_circles WHERE circle_id = 1) ORDER BY id ASC;

3) SELECT COUNT(*) AS `reply_count` FROM replies PARTITION(Jun2017)  WHERE created BETWEEN '2017-05-01 00:00:00' AND '2017-05-31 23:59:59' AND account_id IN (SELECT account_id from account_circles WHERE circle_id = 1) ORDER BY id ASC;


## READ (NO AGGREGATION) ##

4) SELECT * FROM replies PARTITION(Jun2017) USE INDEX (created_time)  WHERE created BETWEEN '2017-05-01 00:00:00' AND '2017-05-31 23:59:59' AND account_id IN (SELECT account_id from account_circles WHERE circle_id = 1) ORDER BY id ASC;

5) SELECT * FROM replies  USE INDEX (created_time)  WHERE created BETWEEN '2017-05-01 00:00:00' AND '2017-05-31 23:59:59' AND account_id IN (SELECT account_id from account_circles WHERE circle_id = 1) ORDER BY id ASC

6) SELECT * FROM replies PARTITION(Jun2017)  WHERE created BETWEEN '2017-05-01 00:00:00' AND '2017-05-31 23:59:59' AND account_id IN (SELECT account_id from account_circles WHERE circle_id = 1) ORDER BY id ASC;


## WRITE ##

7) INSERT INTO `replies` VALUES 
(NULL,'message reply','m_mid.1411384099028:c9d9641d2d4ed87b72','m_mid.1411384234460:438eddd78966c63587','message',1,1,'10154448717860277',1,1411384237,'2017-05-20 17:10:37','2017-05-20 17:10:37');

8) INSERT INTO `replies` PARTITION(Jun2017) VALUES 
(NULL,'message reply','m_mid.1411384099028:c9d9641d2d4ed87b72','m_mid.1411384234460:438eddd78966c63587','message',1,1,'10154448717860277',1,1411384237,'2017-05-20 17:10:37','2017-05-20 17:10:37');

## JOIN ##

9) select actions.*, replies.id, replies.activity_id, replies.customer_replied_from, replies.reply_object 
from actions left join replies
on actions.activity_id = replies.activity_id
WHERE actions.user_id = 93
AND actions.type = 'reply'
AND actions.created BETWEEN '2017-05-01 00:00:00' AND '2017-05-31 23:59:59'
AND actions.account_id IN (SELECT account_id from account_circles WHERE circle_id = '9')
AND replies.reply_object is null

10) select actions.*, replies.id, replies.activity_id, replies.customer_replied_from, replies.reply_object 
from actions left join replies PARTITION(Jun2017)
on actions.activity_id = replies.activity_id
WHERE actions.user_id = 93
AND actions.type = 'reply'
AND actions.created BETWEEN '2017-05-01 00:00:00' AND '2017-05-31 23:59:59'
AND actions.account_id IN (SELECT account_id from account_circles WHERE circle_id = '9')
AND replies.reply_object is null


## READ Results ##

* Table: replies, Total Rows: ~5.7m

Q#  | Partitioned By              | Index Used                    |READ Duration(s)|Rows compared in 'Replies' table  |
----| --------------------------: | -----------------------------:|---------------:|---------------------------------:|
1   | No                          | CREATED                       |0.44 - 0.53     |290020                            | -> existing
5   | No                          | CREATED                       |1.44 - 1.96     |290020                            | -> existing
2   | CREATED(range/3 month)      | CREATED                       |0.68 - 0.74     |391795                            |
3   | CREATED(range/3 month)      | No                            |0.63 - 0.73     |391795                            | (dropping index from table)
4   | CREATED(range/3 month)      | CREATED                       |1.23 - 1.37     |391795                            |
6   | CREATED(range/3 month)      | No                            |0.00 - 0.01     |48360                             | (without dropping index from table)
3   | CREATED(range/3 month)      | No                            |0.00 - 0.01     |48360                             | (without dropping index from table)


* Table: replies, Total Rows: ~30m

Q#  | Partitioned By              | Index Used                    |READ Duration(s)|Rows compared in 'Replies' table  |
----| --------------------------: | -----------------------------:|---------------:|---------------------------------:|
1   | No                          | CREATED                       |2.74 - 3.03     |1256970                           | -> existing
5   | No                          | CREATED                       |4.13 - 4.49     |1256970                           | -> existing
2   | CREATED(range/3 month)      | CREATED                       |2.92 - 3.17     |1623208                           |
3   | CREATED(range/3 month)      | No                            |2.90 - 3.14     |1632208                           | (dropping index from table)
4   | CREATED(range/3 month)      | CREATED                       |3.95 - 4.27     |1632208                           |
6   | CREATED(range/3 month)      | No                            |0.01 - 0.02     |171082                            | (without dropping index from table)
3   | CREATED(range/3 month)      | No                            |0.00 - 0.01     |171082                            | (without dropping index from table)


## WRITE Results ##

* Table: replies, Total Rows: 22957814
* Insertion: 10K row
* Duration:  In both cases (partition/no partition) it is between 10-11(s).


## JOIN Results ##

* Table: replies, Total Rows: ~5.7m
* Table: actions, Total Rows: ~15m

Q#  | Partitioned By              |READ Duration(s)        |Rows compared            |
----| --------------------------: |-----------------------:|------------------------:|
9   | No                          |1m15.90s - 1m24.98s     |1(replies), 918(actions) | 
10  | CREATED(range/3 month)      |1m21.15s - 1m26.02s     |1(replies), 918(actions) |


## Observations ##

Replies table: ~30m rows

* For SELECT query having AGGREGATION function: Both (current structure & partioned by 3 month) performs nearly same.
* For SELECT query: for partiitoned table 3.95 - 4.27(s) & for non partitioned table 4.13 - 4.49(s).
* For INSERT: With 10K rows insertion, duration was same in both cases (i.e 10-11s).
* For JOIN query: for partiitoned table (1m21.15s - 1m26.02s) & for non partitioned table (1m15.90s - 1m24.98s).

* Best results come when we have 'partitioned table having 3 months data', 'table has secondary indexes declared in schema(other than the column that we have used for partitioning)'.
  Interesting fact is, even if we dont specify 'USING INDEX' in our SELECT query & we have some index definition in our partitioned table, that also perform far better. For both 
  AGGREGATION type SELECT queries the duration was 0.00 - 0.01s & for normal SELECT * query it was 0.01 - 0.02s.



## Constraints/Restrictions/Considerations ##

* In case of partitioning by a column, it must be part of the primary key, which eventually introduces composite primary key. Same goes for unique key.

* max number of rows a table can hold: In InnoDB, with a limit on table size of 64 terabytes and a MySQL row-size limit of 65,535 there can be 1,073,741,824 rows. That would be minimum number of records utilizing maximum row-size limit. However, more records can be added if the row size is smaller.

* min number of rows in a table to be a potential candidate for partitioning: at least a million.

* max number of partitions possible:  Prior to MySQL 5.6.7, the maximum possible number of partitions for a given table not using the NDB storage engine was 1024. Beginning with MySQL 5.6.7, this limit is increased to 8192 partitions. Regardless of the MySQL Server version, this maximum includes subpartitions.