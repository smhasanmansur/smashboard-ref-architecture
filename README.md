# Smashboard Test Architecture #

 
![image](https://user-images.githubusercontent.com/6382095/30797863-485e960c-a1f9-11e7-8ce3-80df633058eb.png)


## Quick notes ##

### Load Balancer Tier ###
NGINX / AWS ELB

### Caching Tier ###
* Two possible options:
	- MySQL query caching: 
		* As this comes up with MySQL, this might be less complex to implement. But as stated in MySQL official documentation, the query cache is deprecated as of MySQL 5.7.20, and is removed in MySQL 8.0.
	- Redis: 
    	* First look up in cache. If found, return to Application.
    	* In case of cache miss, look up in MySQL/NoSQL, update cache & return data to Application.
    	* There should be TTL for the records in cache.


### DB Tier ###
* Replication
	- WRITE in MySQL master.
	- READ (transactional) from MySQL replicas.
	- READ (analytics/report) from NoSQL db.
	- There need to be index/reindex/unindexing in NoSQL for WRITEs in MySQL master.
	- NoSQL might be something like ElasticSearch.

### Others ###
* There might be MySQL query router in between Applicaiton Tier & Caching Tier to route as per READ/WRITE.
* There might be something like a message broker (RabbitMQ) to coordinate between different operations in MySQL, Cache(Redis), ElasticSearch(NoSQL) & make it more event driven.
* Another option for horizontal database scaling is database sharding. But applications that rely on cross-table joins are not good candidates for sharding.